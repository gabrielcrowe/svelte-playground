FROM mhart/alpine-node:12

MAINTAINER Lee Nattress, lee.nattress@leighton.com

WORKDIR /docker-container
COPY . .

RUN npm install

EXPOSE 3000
CMD ["node", "server.js"]

#build:
#docker build -t leighton/leighton-dashboard .

#run:
#docker run -p 3000:3000 -d leighton/leighton-dashboard .
