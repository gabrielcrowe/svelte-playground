const Bitbucket = require('bitbucket');
const clientOptions = {
  baseUrl: 'https://api.bitbucket.org/2.0',
  options: {
    timeout: 10000
  }
}
const bitbucket = new Bitbucket(clientOptions)
bitbucket.authenticate({
  type: 'basic',
  username: 'leenattress',
  password: 'Cy3NuNJfjSGxKPayTDYq'
});

var ip = require('ip');
var publicPort = 3000;

//we will cache requests with this
var mcache = require('memory-cache');
var cacheDefault = 30 * 1000; //30 seconds

var cors = require('cors');
var express = require("express");
var app = express();
app.use(cors()); //all the cors!

//folder for build svelte content
app.use(express.static('public'));

app.listen(publicPort, () => {
 console.log(`

--==## 🌈  Server running on ->  http://${ip.address()}:${publicPort} ##==--

`);
});

app.get("/deployments", async (req, res, next) => {
  try {

    const username = req.query.username;
    const repo_slug = req.query.repo_slug;

    //cache key generate
    const key = `/deployments ${username} ${repo_slug}`;

    //get the cache
    let cachedBody = mcache.get(key);
    if (cachedBody) {
      //send cache contents
      res.json(cachedBody);
      console.log(`🍎  CACHED [ ${new Date().toISOString()} ] /deployments (${username}, ${repo_slug})`);
    } else {
      //get fresh, cache and send
      let bbReturn = await bitbucket.repositories.listDeployments({ username, repo_slug });
      mcache.put(key, bbReturn, cacheDefault); //set cache
      res.json(bbReturn); //return dat
      console.log(`🍏  CALLED [ ${new Date().toISOString()} ] /deployments (${username}, ${repo_slug})`);
    }


    let bbReturn = await bitbucket.repositories.listDeployments({ username, repo_slug });
    res.json(bbReturn.data.values);
    console.log(`🌈  [ ${new Date().toISOString()} ] /deployments (${username}, ${repo_slug})`);
  } catch (e) {
    res = { error: e }
    next(e)
  }
});

app.get("/pipelines", async (req, res, next) => {
  try {

    const username = req.query.username;
    const repo_slug = req.query.repo_slug

    //cache key generate
    const key = `/pipelines ${username} ${repo_slug}`;

    //get the cache
    let cachedBody = mcache.get(key);
    if (cachedBody) {
      //send cache contents
      res.json(cachedBody);
      console.log(`🍎  CACHED [ ${new Date().toISOString()} ] /pipelines (${username}, ${repo_slug})`);
    } else {
      //get fresh, cache and send
      let bbReturn = await bitbucket.repositories.listPipelines({ username, repo_slug, sort: '-created_on' });
      mcache.put(key, bbReturn, cacheDefault); //set cache
      res.json(bbReturn); //return dat
      console.log(`🍏  CALLED [ ${new Date().toISOString()} ] /pipelines (${username}, ${repo_slug})`);
    }

  } catch (e) {
    res = { error: e }
    next(e)
  }
});


app.get("/pulls", async (req, res, next) => {
  try {

    const username = req.query.username;
    const repo_slug = req.query.repo_slug;

    //cache key generate
    const key = `/pulls ${username} ${repo_slug}`;

    //get the cache
    let cachedBody = mcache.get(key);
    if (cachedBody) {
      //send cache contents
      res.json(cachedBody);
      console.log(`🍎  CACHED [ ${new Date().toISOString()} ] /pulls (${username}, ${repo_slug})`);
    } else {
      //get fresh, cache and send
      let bbReturn = await bitbucket.repositories.listPullRequests({ username, repo_slug, sort: 'created_on' }); //do api call
      mcache.put(key, bbReturn, cacheDefault); //set cache
      res.json(bbReturn); //return dat
      console.log(`🍏  CALLED [ ${new Date().toISOString()} ] /pulls (${username}, ${repo_slug})`);
    }

  } catch (e) {
    res = { error: e }
    next(e)
  }
});
